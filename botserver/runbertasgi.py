#python botserver/runbertasgi.py


import pandas as pd
import numpy as np
import json
import re
import logging

import torch
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
from torch.utils.data import Dataset, DataLoader

from transformers import BertModel, BertTokenizer
from transformers import BertForSequenceClassification, BertConfig

from fastapi import FastAPI
import uvicorn
from pydantic import BaseModel


PORT = 5001


dataset = pd.read_csv('data/mldata/asktbl.tsv',
                      sep='\t', index_col=False, header=None, names=['utterance', 'label'])
# dataset.to_csv('mlchatclassification.tsv', sep='\t', index=False, header=False)


label_to_ix = {}
for label in dataset.label:
    for word in label.split():
        if word not in label_to_ix:
            label_to_ix[word] = len(label_to_ix)
label_to_ix

# Loading RoBERTa classes

config = BertConfig.from_pretrained('bert-base-finnish-cased-v1')
config.num_labels = len(list(label_to_ix.values()))
# config

tokenizer = BertTokenizer.from_pretrained('bert-base-finnish-cased-v1')
model = BertForSequenceClassification(config)


def prepare_features(seq_1, max_seq_length=300,
             zero_pad=False, include_CLS_token=True, include_SEP_token=True):
    # Tokenzine Input
    tokens_a = tokenizer.tokenize(seq_1)

    # Truncate
    if len(tokens_a) > max_seq_length - 2:
        tokens_a = tokens_a[0:(max_seq_length - 2)]
    # Initialize Tokens
    tokens = []
    if include_CLS_token:
        tokens.append(tokenizer.cls_token)
    # Add Tokens and separators
    for token in tokens_a:
        tokens.append(token)

    if include_SEP_token:
        tokens.append(tokenizer.sep_token)

    input_ids = tokenizer.convert_tokens_to_ids(tokens)
    # Input Mask
    input_mask = [1] * len(input_ids)
    # Zero-pad sequence lenght
    if zero_pad:
        while len(input_ids) < max_seq_length:
            input_ids.append(0)
            input_mask.append(0)
    return torch.tensor(input_ids).unsqueeze(0), input_mask


class Intents(Dataset):
    def __init__(self, dataframe):
        self.len = len(dataframe)
        self.data = dataframe
        
    def __getitem__(self, index):
        utterance = self.data.utterance[index]
        label = self.data.label[index]
        X, _  = prepare_features(utterance)
        y = label_to_ix[self.data.label[index]]
        return X, y
    
    def __len__(self):
        return self.len   


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


model_path = 'models/chatbertclass.pth'

model.load_state_dict(torch.load(model_path, map_location=device))

def get_reply(model,msg):
  model.eval()
  input_msg, _ = prepare_features(msg)
  if torch.cuda.is_available():
    input_msg = input_msg.cuda()
  output = model(input_msg)[0]
  val, pred_label = torch.max(output.data, 1)  
  prediction=list(label_to_ix.keys())[pred_label]
  accuracy = val.tolist()[0]

  return accuracy,prediction


class Itemask(BaseModel):
    ask: str


app = FastAPI()

@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.post("/translator/translate")
def predict(item: Itemask):
    ask = item.ask

    print('ASK',ask)
    accuracy,tran = get_reply(model,ask)
    return {'Accuracy':accuracy,'Answer':tran}

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=PORT,log_level="info", reload=False)    
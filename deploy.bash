
# conda create -n bert python=3.7
# mkdir -p repos/gitlab.com/remotejob/mlbottransformers
# mkdir modelsp 
# mkdir botserver


# scp models/chatbertclass.pth root@159.203.67.26:repos/gitlab.com/remotejob/mlbottransformers/models/new.pth && \
# ssh root@159.203.67.26 systemctl stop bert.service && \
# ssh root@159.203.67.26 cp repos/gitlab.com/remotejob/mlbottransformers/models/new.pth repos/gitlab.com/remotejob/mlbottransformers/models/chatbertclass.pth && \
# scp data/mldata/asktbl.tsv root@159.203.67.26:repos/gitlab.com/remotejob/mlbottransformers/data/mldata/ && \
# ssh root@159.203.67.26 systemctl start bert.service


#scp botserver/runbertasgi.py root@159.203.67.26:repos/gitlab.com/remotejob/mlbottransformers/botserver/

# scp data/mldata/asktbl.tsv root@159.203.67.26:repos/gitlab.com/remotejob/mlbottransformers/data/mldata/
# ssh root@159.203.67.26 systemctl start bert.service


conda activate kaggle
rm -rf output/*

export KAGGLE_CONFIG_DIR=/home/juno/kaggleremotejob
kaggle kernels output remotejob/mlbottransformers -p output/
cp output/chatbertclass.pth models/

export KAGGLE_CONFIG_DIR=/home/juno/kagglealmazseo
kaggle kernels output almazseo/mlbottransformers -p output/
cp output/chatbertclass.pth models/

export KAGGLE_CONFIG_DIR=/home/juno/kaggleipotecafi
kaggle kernels output ipotecafi/mlbottransformers -p output/
cp output/chatbertclass.pth models/


python botserver/runbertasgi.py 

scp data/mldata/asktbl.tsv models/chatbertclass.pth  root@159.203.67.26:/tmp/
ssh root@159.203.67.26 /root/upgrade_mlbottransformers.bash


on ~/gowork/src/gitlab.com/remotejob/chatproxynlp !!! change SCORE!!!
make deploy 
conda activate kaggle

export KAGGLE_CONFIG_DIR=/home/juno/kaggleremotejob
cp datasets/dataset-metadata.json.remotejob data/dataset-metadata.json
#kaggle datasets create -r zip -p data/
kaggle datasets version -r zip -p data/ -m "Updated data 0"

export KAGGLE_CONFIG_DIR=/home/juno/kaggleipotecafi
cp datasets/dataset-metadata.json.ipotecafi data/dataset-metadata.json
#kaggle datasets create -r zip -p data/
kaggle datasets version -r zip -p data/ -m "Updated data 0"


export KAGGLE_CONFIG_DIR=/home/juno/kagglealmazseo
cp datasets/dataset-metadata.json.almazseo data/dataset-metadata.json
#kaggle datasets create -r zip -p data/
kaggle datasets version -r zip -p data/ -m "Updated data 0"

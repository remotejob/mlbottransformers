
import pandas as pd
from sklearn.model_selection import train_test_split
df=pd.read_csv('data/mldata/askorg.csv',sep='\t')

df['labels'] = list(zip(df.greetings.tolist(),df.goodbye.tolist()))
df['text'] = df['comment_text'].apply(lambda x: x.replace('\n', ' '))

print(df.head())

train_df, eval_df = train_test_split(df, test_size=0.2)

# # train_df = pd.DataFrame(train_data)

# print(train_df.head())

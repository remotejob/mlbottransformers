# -*- coding: utf-8 -*-
"""Text_Classification_RoBERTa.ipynb

Original file is located at
    https://colab.research.google.com/drive/1xg4UMQmXjDik3v9w-dAsk4kq7dXX_0Fm
"""
import os
import pandas as pd
import numpy as np
import json, re
from tqdm import tqdm_notebook
# from uuid import uuid4
from logging import getLogger, Formatter, FileHandler, StreamHandler, INFO, DEBUG

## Torch Modules
import torch
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
from torch.utils.data import Dataset, DataLoader

os.system('pip install transformers')
os.system('pip install pytorch-transformers')

## PyTorch Transformer
from transformers import BertModel, BertTokenizer
from transformers import BertForSequenceClassification, BertConfig


def create_logger(exp_version):
    log_file = ("{}.log".format(exp_version))

    # logger
    logger_ = getLogger(exp_version)
    logger_.setLevel(DEBUG)

    # formatter
    fmr = Formatter("[%(levelname)s] %(asctime)s >>\t%(message)s")

    # file handler
    fh = FileHandler(log_file)
    fh.setLevel(DEBUG)
    fh.setFormatter(fmr)

    # stream handler
    ch = StreamHandler()
    ch.setLevel(INFO)
    ch.setFormatter(fmr)

    logger_.addHandler(fh)
    logger_.addHandler(ch)


def get_logger(exp_version):
    return getLogger(exp_version)

VERSION = "001" #
create_logger(VERSION)

dataset =pd.read_csv('/kaggle/input/mlbottransformersdata/asktbl.tsv',sep='\t', index_col=False, header=None,names = ['utterance', 'label'])
# dataset.to_csv('mlchatclassification.tsv', sep='\t', index=False, header=False)


label_to_ix = {}
for label in dataset.label:
    for word in label.split():
        if word not in label_to_ix:
            label_to_ix[word]=len(label_to_ix)
label_to_ix

## Loading RoBERTa classes

config = BertConfig.from_pretrained('bert-base-finnish-cased-v1')
config.num_labels = len(list(label_to_ix.values()))
# config

tokenizer = BertTokenizer.from_pretrained('bert-base-finnish-cased-v1')
model = BertForSequenceClassification(config)

## Feature Preparation

def prepare_features(seq_1, max_seq_length = 300, 
             zero_pad = False, include_CLS_token = True, include_SEP_token = True):
    ## Tokenzine Input
    tokens_a = tokenizer.tokenize(seq_1)

    ## Truncate
    if len(tokens_a) > max_seq_length - 2:
        tokens_a = tokens_a[0:(max_seq_length - 2)]
    ## Initialize Tokens
    tokens = []
    if include_CLS_token:
        tokens.append(tokenizer.cls_token)
    ## Add Tokens and separators
    for token in tokens_a:
        tokens.append(token)

    if include_SEP_token:
        tokens.append(tokenizer.sep_token)

    input_ids = tokenizer.convert_tokens_to_ids(tokens)
    ## Input Mask 
    input_mask = [1] * len(input_ids)
    ## Zero-pad sequence lenght
    if zero_pad:
        while len(input_ids) < max_seq_length:
            input_ids.append(0)
            input_mask.append(0)
    return torch.tensor(input_ids).unsqueeze(0), input_mask

## Dataset Loader Classes

class Intents(Dataset):
    def __init__(self, dataframe):
        self.len = len(dataframe)
        self.data = dataframe
        
    def __getitem__(self, index):
        utterance = self.data.utterance[index]
        label = self.data.label[index]
        X, _  = prepare_features(utterance)
        y = label_to_ix[self.data.label[index]]
        return X, y
    
    def __len__(self):
        return self.len

train_size = 0.8
train_dataset=dataset.sample(frac=train_size,random_state=200).reset_index(drop=True)
test_dataset=dataset.drop(train_dataset.index).reset_index(drop=True)

print("FULL Dataset: {}".format(dataset.shape))
print("TRAIN Dataset: {}".format(train_dataset.shape))
print("TEST Dataset: {}".format(test_dataset.shape))

training_set = Intents(train_dataset)
testing_set = Intents(test_dataset)

training_set.__getitem__(0)[0].shape

model(training_set.__getitem__(0)[0])

## Training Params

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model = model.cuda()

# Parameters
params = {'batch_size': 1,
          'shuffle': True,
          'drop_last': False,
          'num_workers': 1}

training_loader = DataLoader(training_set, **params)
testing_loader = DataLoader(testing_set, **params)

loss_function = nn.CrossEntropyLoss()
learning_rate = 1e-05
optimizer = optim.Adam(params =  model.parameters(), lr=learning_rate)

## Test Forward Pass
inp = training_set.__getitem__(0)[0].cuda()
output = model(inp)[0]
print(output.shape)

max_epochs = 200 #100
model = model.train()
for epoch in tqdm_notebook(range(max_epochs)):
    print("EPOCH -- {}".format(epoch))
    for i, (sent, label) in enumerate(training_loader):
        optimizer.zero_grad()
        sent = sent.squeeze(0)
        if torch.cuda.is_available():
          sent = sent.cuda()
          label = label.cuda()
        output = model.forward(sent)[0]
        _, predicted = torch.max(output, 1)
        
        loss = loss_function(output, label)
        loss.backward()
        optimizer.step()
        
        if i%100 == 0:
            correct = 0
            total = 0
            for sent, label in testing_loader:
                sent = sent.squeeze(0)
                if torch.cuda.is_available():
                  sent = sent.cuda()
                  label = label.cuda()
                output = model.forward(sent)[0]
                _, predicted = torch.max(output.data, 1)
                total += label.size(0)
                correct += (predicted.cpu() == label.cpu()).sum()
            accuracy = 100.00 * correct.numpy() / total
            print('Iteration: {}. Loss: {}. Accuracy: {}%'.format(i, loss.item(), accuracy))

torch.save(model.state_dict(), 'chatbertclass.pth')

model_path = 'chatbertclass.pth'

model.load_state_dict(torch.load(model_path, map_location=device))


def get_reply(msg):
  model.eval()
  input_msg, _ = prepare_features(msg)
  if torch.cuda.is_available():
    input_msg = input_msg.cuda()
  output = model(input_msg)[0]
  val, pred_label = torch.max(output.data, 1)  
  prediction=list(label_to_ix.keys())[pred_label]
  accuracy = val.tolist()[0]

  return accuracy,prediction


with open("0mlresult.txt", "w+") as f:
    file1 = open('/kaggle/input/mlbottransformersdata/testask.tsv', 'r') 
    Lines = file1.readlines()
    allacc = 0
    count = 0
    for line in Lines:
        accu,pred = get_reply(line)
        allacc += accu
        count += 1
        f.write(line.strip() +"\t"+pred+"\t"+str(accu)+"\n")

    f.write("allacc:" +str(allacc/count))        

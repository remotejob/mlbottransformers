

import os
# import sys
import shutil

shutil.copy('/kaggle/input/mlbottransformersdata/train.csv','/kaggle/working/')
shutil.copy('/kaggle/input/mlbottransformersdata/val.csv','/kaggle/working/')
shutil.copy('/kaggle/input/mlbottransformersdata/labels.csv','/kaggle/working/')

# os.chdir('/kaggle/input/mlbottransformersdata')
os.system('git clone https://github.com/NVIDIA/apex')
os.system('pip install -v --no-cache-dir --global-option="--cpp_ext" --global-option="--cuda_ext" ./apex')
# shutil.rmtree('./apex')

os.system('pip install --no-cache-dir -U fast-bert')


from fast_bert.data_cls import BertDataBunch

databunch = BertDataBunch('/kaggle/working/', '/kaggle/working/',
                          tokenizer='bert-base-uncased',
                          train_file='train.csv',
                          val_file='val.csv',
                          label_file='labels.csv',
                          label_col="label",
                          text_col='comment_text',
                          batch_size_per_gpu=16,
                          max_seq_length=512,
                          multi_gpu=False,
                          multi_label=True,
                          model_type='bert')

import torch
import apex
from fast_bert.learner_cls import BertLearner
from fast_bert.metrics import accuracy
import logging


logger = logging.getLogger()
device_cuda = torch.device("cuda")
metrics = [{'name': 'accuracy', 'function': accuracy}]

learner = BertLearner.from_pretrained_model(
						databunch,
						pretrained_path='bert-base-uncased',
						metrics=metrics,
						device=device_cuda,
						logger=logger,
						output_dir='/kaggle/working/',
						finetuned_wgts_path=None,
						warmup_steps=500,
						multi_gpu=False,
						is_fp16=True,
						multi_label=True,
						logging_steps=50)


learner.fit(epochs=6,
			lr=6e-5,
			validate=True, 	# Evaluate the model after each epoch
			schedule_type="warmup_cosine",
			optimizer_type="lamb")

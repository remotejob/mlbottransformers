import os
import sys
import shutil

# os.chdir('/kaggle/input/mlbottransformersdata')
os.system('git clone https://github.com/NVIDIA/apex')
os.system('pip install -v --no-cache-dir --global-option="--cpp_ext" --global-option="--cuda_ext" ./apex')
shutil.rmtree('./apex')
# os.chdir('/kaggle/working')

os.system('pip install --no-cache-dir -U simpletransformers')

import pandas as pd
from sklearn.model_selection import train_test_split
from simpletransformers.classification import MultiLabelClassificationModel

df = pd.read_csv('/kaggle/input/mlbottransformersdata/askorg.csv', sep='\t')

df['labels'] = list(zip(df.greetings.tolist(), df.goodbye.tolist(), df.masturbation.tolist(
), df.robot.tolist(), df.i_call_if.tolist(), df.my_phone_number.tolist()))
df['text'] = df['comment_text'].apply(lambda x: x.replace('\n', ' '))

df = pd.DataFrame(df, columns=['text', 'labels'])

print(df.head())

train_df, eval_df = train_test_split(df, test_size=0.2)

# Create a MultiLabelClassificationModel
# model = MultiLabelClassificationModel('roberta', 'roberta-base', num_labels=6, args={
#   'reprocess_input_data': True, 'overwrite_output_dir': True, 'num_train_epochs': 10})

# model = MultiLabelClassificationModel('roberta', 'roberta-base', num_labels=6, args={
#                                       'train_batch_size': 2, 'gradient_accumulation_steps': 16, 'learning_rate': 3e-5, 'num_train_epochs': 6, 'max_seq_length': 512})

model = MultiLabelClassificationModel('bert', 'bert-base-multilingual-cased', num_labels=6, args={
                                      'train_batch_size': 2, 'gradient_accumulation_steps': 16, 'learning_rate': 3e-5, 'num_train_epochs': 5, 'max_seq_length': 512})



# You can set class weights by using the optional weight argument
# print(train_df.head())

model.train_model(train_df)

# Evaluate the model
result, model_outputs, wrong_predictions = model.eval_model(eval_df)
print(result)
print(model_outputs)

predictions, raw_outputs = model.predict(
    ['soita mulle'])
print(predictions)
print(raw_outputs)


with open("0mlresult.txt", "w+") as f:
   for listitem in predictions:
        f.write('%s\n' % listitem)

predictions, raw_outputs = model.predict(
    ['oot robotti'])
print(predictions)
print(raw_outputs)


with open("1mlresult.txt", "w+") as f:
   for listitem in predictions:
        f.write('%s\n' % listitem)


# f.write(result +'\r\n')
# f.write(model_outputs +'\r\n')
# f.write(wrong_predictions+'\r\n')
# f.write(predictions)
# f.write(raw_outputs+'\r\n')
# f.close()
